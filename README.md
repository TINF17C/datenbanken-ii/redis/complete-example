# Redis Example Project

This is the example project used for the presentation about Redis by Bastian Krug, Phuc and Michael Schweiker.

## Steps to run the project
1. Get a working instance of redis [Download](https://redis.io/download)
2. Clone this project
3. Open `frontend`-subfolder in commandline and execute `gradlew run`
4. Open browser and point it to [localhost:8080](http://localhost:8080)
5. Open `backend`-subfolder in another commandline and execute `gradlew run`

## More backend options
The runnable jar is located in `backend/build/libs`.
It could be run like any other java application.
The possible options are:
```
--help              Show this help

--rebuild           Reset the database
--extract           only used when --rebuild is set, extracts the race with id 1030
--target FILENAME   only used when --extract is set, specifies file for extracted data, defaults to output.csv

--file   FILENAME   specify which file contains current race, if not set output.csv is used
                    when --extract and --target are set, --target will be used
```
